package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"gitlab.com/garbotron/donkeytownsfolk"
	"gitlab.com/garbotron/goshots"
	"log"
	"net/http"
	_ "net/http/pprof"
)

const httpPort = 80

func main() {

	r := mux.NewRouter()

	if err := goshots.Init(r); err != nil {
		log.Fatal(err)
		return
	}

	if err := donkeytownsfolk.Init(r); err != nil {
		log.Fatal(err)
		return
	}

	http.Handle("/", r)
	http.ListenAndServe(fmt.Sprintf(":%d", httpPort), nil)
}
